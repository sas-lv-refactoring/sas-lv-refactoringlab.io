# SAS LabVIEW Refactoring Tools Documentation

## Getting Started

{{ get_tool_data() }}

### About

This is a set of tools for common refactoring actions in LabVIEW. It is centered around classes and interfaces.

### Why?

Refactoring in LabVIEW is hard, but it shouldn't be.

### What is refactoring? 

Glad you asked. Refactoring is changing the internal structure of your code without changing it's observable behavior. Why would you want to do that? Well, have you ever inherited a mess of spaghetti code? Well then you'll know that code can sometimes be hard to read, even if it is functional. The goal of refactoring is to take functioning code that is hard to read, understand, or extend and make it easier to read, understand or extend while at the same time guaranteeing the same existing functionality stays intact. Want to know more? Martin Fowler wrote a book about it. I highly suggest you read it.

### Installation

Each of these G-CLI Tools is available on VIPM. That is the easiest way to install them. 

Here are the links to download the latest packages from VIPM.
{% for tool in tools() %}
* [![stars badge]( {{ tool["vipm_badge_stars"] }})]( {{ tool['vipm_url'] }})  [![installs badge]( {{ tool["vipm_badge_installs"] }})]( {{ tool["vipm_url"] }} ) [{{ tool['official_name'] }}]({{ tool['vipm_url'] }}) 
{% endfor %}

### Liscensing

All of the tools are licensed under the [MIT license](https://opensource.org/license/mit/t p). A license file is included in each VIPM installation package.

### Verifying G-CLI Works

### Supported Versions

- LabVIEW -These tools are packaged with VIPM using LabVIEW 2020, so they should work with any LabVIEW bitness, version 2020 or later. 
- OS - These tools are Windows only at the moment, although the latest G-CLI apparently also works on Linux, so perhaps that may work. If you try it and find out, please let me know. I'm still on Windows 10, but there is no reason they shouldn't work with Windows 11.
- G-CLI - 2.4.0.4 or later.

## Available Commands

#### Description 

#### Options

#### Example

#### Repository

For more information and to see the source code, check out the repository here:

### Introductory Videos
COMING SOON

<!-- 100% privacy friendly analytics -->
<script async defer src="https://scripts.simpleanalyticscdn.com/latest.js"></script>
<noscript><img src="https://queue.simpleanalyticscdn.com/noscript.gif" alt="" referrerpolicy="no-referrer-when-downgrade" /></noscript>