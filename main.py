from utils import get_content, get_section
from config import TOOLS


def define_env(env):
    @env.macro
    def tools():
        return (tool for tool in TOOLS)

    @env.macro
    def get_tool_data():
        for tool in TOOLS:
            readme_content = get_content(tool["base_url"] + "/-/raw/main/README.md")
            for item in ["description", "short_description", "options", "example"]:
                tool[item] = get_section(readme_content, item)
            tool["vipm_badge_installs"] = (
                tool["vipm_url"] + "/badge.svg?metric=installs"
            )
            tool["vipm_badge_stars"] = tool["vipm_url"] + "/badge.svg?metric=stars"
            tool["gitlab_release_badge"] = tool["base_url"] + "/-/badges/release.svg"
            tool["gitlab_release_url"] = tool["base_url"] + "/-/releases"
        return ""
