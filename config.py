TOOLS = [
    {
        "cmd": "clearlvcache",
        "official_name":"Clear LabVIEW Cache For G-CLI",
        "base_url": "https://gitlab.com/sas-gcli-tools/clearlvcache",
        "vipm_url": "https://www.vipm.io/package/sas_workshops_lib_clearlvcache_for_g_cli",
    },
    {
        "cmd": "vipc",
        "official_name": "VIPC Applier For G-CLI",
        "base_url": "https://gitlab.com/sas-gcli-tools/vipc",
        "vipm_url": "https://www.vipm.io/package/sas_workshops_lib_vipc_applier_for_g_cli"
    },
    {
        "cmd": "lvbuildspec",
        "official_name": "LVBuildSpec For G-CLI",
        "base_url": "https://gitlab.com/sas-gcli-tools/lvbuildspec",
        "vipm_url": "https://www.vipm.io/package/sas_workshops_lib_lvbuildspec_for_g_cli"
    },
    {
        "cmd": "vitester",
        "official_name": "VITester For G-CLI",
        "base_url": "https://gitlab.com/sas-gcli-tools/vitester",
        "vipm_url": "https://www.vipm.io/package/sas_workshops_lib_vitester_for_g_cli"
    },
]
