import re
import urllib.request


def get_section(text, section):
    start = f"<!-- start {section} -->"
    end = f"<!-- end {section} -->"
    if start in text and end in text:
        output = text.split(start)[1].split(end)[0].strip()
    else:
        output = f"Section {section} not found"
    return output


def test_get_section():
    input = """# clearlvcache

<!-- start short_description -->
A G-CLI tool for clearing the LabVIEW Cache.
<!-- end short_description -->

[TOC]

## Description
<!-- start description -->
This tool clears both the AppBuilder and CompiledObject Caches
<!-- end description -->

## Options
<!-- start options -->
This G-CLI has no options. You just call it directly from G-CLI.
<!-- end options -->

## Example

<!-- start example -->
```bash
# G-CLI options ignored for simplicity.
# it is really this simple.
g-cli clearlvcache

```
<!-- end example -->
"""
    assert (
        get_section(input, "description")
        == "This tool clears both the AppBuilder and CompiledObject Caches"
    )
    assert (
        get_section(input, "options")
        == "This G-CLI has no options. You just call it directly from G-CLI."
    )


def get_content(link):
    f = urllib.request.urlopen(link)
    myfile = f.read()
    return myfile.decode("utf-8")
